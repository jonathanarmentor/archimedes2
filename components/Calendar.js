import { Calendar as Cal, Alert } from 'antd'
import moment from 'moment'

class Calendar extends React.Component {
	dateFormat = 'MM-DD-YYYY'
	defaultDate = moment('1994-09-17')
	state = {
		value: this.defaultDate,
		selectedValue: this.defaultDate
	}
	onSelect = (value) => {
		this.setState({
			value,
			selectedValue: value
		})
	}
	onPanelChange = (value) => {
		this.setState({ value })
	}

	render() {
		const { value, selectedValue } = this.state
		return (
			<div className="Calendar">
				<Alert message={`You selected date: ${selectedValue && selectedValue.format(this.dateFormat)}`} />
				<Cal value={value} onSelect={this.onSelect} onPanelChange={this.onPanelChange} />
			</div>
		)
	}
}

export default Calendar
