import { Layout, Menu, Icon } from 'antd'
import Link from 'next/link'

export const DashboardNav = () => {
	const MenuItem = (item) => (
		<Menu.Item key={item.key}>
			<a>
				<Link href={item.to}>{item.title}</Link>
			</a>
		</Menu.Item>
	)
	let DashMenu = {
		content: [
			{
				key: 'email',
				title: (
					<span>
						<Icon type="mail" />E-Blasts
					</span>
				),
				items: [
					{
						key: 'email1',
						to: '/email/create',
						title: (
							<span>
								<Icon type="plus" />Compose
							</span>
						)
					},
					{
						key: 'email2',
						to: '/email/schedule',
						title: (
							<span>
								<Icon type="schedule" />Schedule
							</span>
						)
					},
					{
						key: 'email3',
						to: '/email/assets',
						title: (
							<span>
								<Icon type="file" />Assets
							</span>
						)
					},
					{
						key: 'email4',
						to: '/email/analytics',
						title: (
							<span>
								<Icon type="dot-chart" />Analytics
							</span>
						)
					},
					{
						key: 'email5',
						to: '/email/contacts',
						title: (
							<span>
								<Icon type="dot-chart" />Contacts
							</span>
						)
					},
					{
						key: 'email6',
						to: '/email/campaigns',
						title: (
							<span>
								<Icon type="list" />Campaigns
							</span>
						)
					},
					{
						key: 'email7',
						to: '/email/lists',
						title: (
							<span>
								<Icon type="dot-chart" />Lists
							</span>
						)
					}
				]
			},
			{
				key: 'sub2',
				title: (
					<span>
						<Icon type="laptop" />subnav 2
					</span>
				),
				items: [
					{
						key: 5,
						to: '#',
						title: 'option1'
					},
					{
						key: 6,
						to: '#',
						title: 'option2'
					},
					{
						key: 7,
						to: '#',
						title: 'option3'
					},
					{
						key: 8,
						to: '#',
						title: 'option4'
					}
				]
			},
			{
				key: 'sub3',
				title: (
					<span>
						<Icon type="notification" />subnav 3
					</span>
				),
				items: [
					{
						key: 9,
						to: '#',
						title: 'option1'
					},
					{
						key: 10,
						to: '#',
						title: 'option2'
					},
					{
						key: 11,
						to: '#',
						title: 'option3'
					},
					{
						key: 12,
						to: '#',
						title: 'option4'
					}
				]
			}
		],
		props: {
			mode: 'inline',
			theme: 'dark',
			defaultSelectedKeys: [ '1' ],
			defaultOpenKeys: [ 'sub1' ],
			style: { height: '100%', borderRight: 0 }
		}
	}
	let { SubMenu } = Menu
	return (
		<Menu {...DashMenu.props}>
			{DashMenu.content.map((child) => (
				<SubMenu key={child.key} title={child.title}>
					{child.items.map((item) => MenuItem(item))}
				</SubMenu>
			))}
		</Menu>
	)
}

export const PrimaryNav = () => (
	<Layout className="header">
		<Menu theme="light" mode="horizontal" defaultSelectedKeys={[ '1' ]} style={{ lineHeight: '64px' }}>
			<Menu.Item key="1">
				<Link href="/">
					<a>ARCHIMEDES</a>
				</Link>
			</Menu.Item>
			<Menu.Item key="2">
				<Link href="/dashboard">
					<a>
						<Icon type="dashboard" />Dashboard
					</a>
				</Link>
			</Menu.Item>
			<Menu.Item key="3">
				<Link href="/users">
					<a>
						<Icon type="user" />Users
					</a>
				</Link>
			</Menu.Item>
			<Menu.Item key="4">
				<Link href="/activity">
					<a>
						<Icon type="rocket" />Activity
					</a>
				</Link>
			</Menu.Item>
			<Menu.Item key="5">
				<Link href="/api">
					<a>
						<Icon type="api" />API
					</a>
				</Link>
			</Menu.Item>
		</Menu>
	</Layout>
)

export default PrimaryNav
