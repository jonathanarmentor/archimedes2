import Head from 'next/head'
import animateStyles from 'animate.css/animate.css'
import stylesheet from 'antd/dist/antd.css'

export default (props) => (
	<div>
		<Head>
			<title>{props.title && props.title + ' | '} Archimedes</title>
			<meta name="viewport" content="initial-scale=1.0, width=device-width" />
			{props.children}
		</Head>
		<style dangerouslySetInnerHTML={{ __html: stylesheet + animateStyles }} />
	</div>
)
