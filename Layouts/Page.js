import { Layout } from 'antd'
import PrimaryNav from '../components/Nav'
import Head from '../components/Head'
import Footer from '../Layouts/Footer'

const Page = (props) => (
	<div style={props.style}>
		<Head title={props.title} />
		<PrimaryNav />
		<Layout>{props.children}</Layout>
		<Footer />
	</div>
)
export default Page
