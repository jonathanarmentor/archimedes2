import dynamic from 'next/dynamic'

const DashBundle = dynamic({
	modules: (props) => {
		const components = {
			List: import('../components/List'),
			Calendar: import('../components/Calendar')
		}

		// Add remove components based on props

		return components
	},
	render: (props, { List, Calendar }) => (
		<div>
			<h1>{props.title}</h1>
			{props.showList && <List />}
			{props.showCal && <Calendar />}
		</div>
	)
})

export default (props) => <DashBundle title="Dynamic Dash Bundle" {...props} />
