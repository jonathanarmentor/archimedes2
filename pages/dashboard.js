import { Layout } from 'antd'
import Page from '../Layouts/Page'
import { DashboardNav as Nav } from '../components/Nav'

class Dashboard extends React.Component {
	render() {
		let { Content, Sider } = Layout
		let Styles = () => (
			<style jsx>
				{`
					.Dashboard {
						min-height: 100vh;
					}

					.Wrapper {
						padding: 24px;
					}
					.Content {
						background: #fff;
						padding: 24px;
						margin: 0;
						min-height: 100vh;
					}
				`}
			</style>
		)
		return (
			<Page>
				<Layout className="Dashboard">
					<Sider className="animated slideInLeft" width={200}>
						<Nav />
					</Sider>
					<Layout className="Wrapper">
						<Content className="Content animated slideInUp">
							<h1>`~~~ TITLE ~~~~`</h1>
							<div>`CONTENT`</div>
						</Content>
					</Layout>
				</Layout>
				<Styles />
			</Page>
		)
	}
}

export default Dashboard
