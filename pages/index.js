import 'isomorphic-unfetch'
import Page from '../Layouts/Page'

class Index extends React.Component {
	render() {
		return (
			<Page>
				<h1>Home!</h1>
			</Page>
		)
	}
}
export default Index
